---
title: 'We Need More tor Nodes'
date: '2022-05-24T02:44:00+00:00'
status: publish
permalink: /we-need-more-tor-nodes
author: 'Ben R'
excerpt: ''
type: post
id: 40
thumbnail: ../uploads/2022/05/download-1.png
category:
    - 'Privacy &amp; Security'
tag: []
post_format: []
---
Tor only currently has 6000 relays and 2000 bridges. Who knows how many of them can easily be made by the government agencies. Tor is our defense towards censorship. And we can start making more relays and bridges to help decentralize the internet.