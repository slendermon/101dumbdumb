---
title: 'More people need websites.'
date: '2022-05-29T14:39:59+00:00'
status: publish
permalink: /more-people-need-websites
author: 'Mr T'
excerpt: ''
type: post
id: 140
category:
    - Uncategorized
tag: []
post_format: []
ao_post_optimize:
    - 'a:6:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";s:15:"ao_post_preload";s:0:"";}'
---
If you’re interested in computers, like me, I think it is actually really fun setting up your own website.